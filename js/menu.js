document.addEventListener("DOMContentLoaded", function () {
    console.log('Your document is ready!');


    if (localStorage.token) {
        console.log(localStorage.token)
    } else {
        alert(`Acceso denegado`)
        location.href = 'index.html'
    }

    var btnSalir = document.getElementById('btnSalir')
    btnSalir.addEventListener('click',logOut)
    }
)


function logOut(e){
    var token = localStorage.token
    console.log(token)
    e.preventDefault()    
               
        localStorage.removeItem('token')

        console.log(localStorage.token)


        location.href='index.html'
}


