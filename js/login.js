document.addEventListener("DOMContentLoaded", function () {
        console.log('Your document is ready!');

        
        var form = document.getElementById('form');
        form.addEventListener('submit', (e) => {
                e.preventDefault();

                var email = document.getElementById('txtEmail').value  
                var password = document.getElementById('txtPassword').value
                var token = true

                var data = {
                    email: email,
                    password: password,
                    token: token

                }

                console.log(data)

                logIn(data);
                //removeHtml(e);

            }
        )}
    )

    function removeHtml(e) {
        var btnLogin = document.querySelector(".kid");
        btnLogin.parentNode.removeChild(btnLogin);
        //setInterval(function(){
        var divLoading = document.getElementById('divLoading');

        divLoading.innerHTML = `
    <div class="preloader-wrapper small active">
        <div class="spinner-layer spinner-green-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
        </div>
    </div>`; 
    }


    function logIn(data) {

        fetch('https://api-flutter.herokuapp.com/api/login', {
                method: 'POST', // or 'PUT'
                body: JSON.stringify(data),
                headers:{
                    'Content-Type': 'application/json'
                } 
            })
            .then(response =>{
                //console.log(res.json)
                return response.json()
            })
            .then(data => {
                //console.log('DATA: ',data)
                if (data.token) {
                    localStorage.setItem('token',data.token)
                    console.log(`TOKEN: ${localStorage.token}`)
                    console.log(`Headers: ${data.status}`)
                   // location.href='menu.html'
                } else {
                     //console.log(`Mensaje: ${data.message}`)
                    alert(`${data.message}`)
                }
            })
            .catch(function () {
                alert(" No se puede conectar al backend");
            })
    }


